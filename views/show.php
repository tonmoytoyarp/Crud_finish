<?php
include_once '../vendor/autoload.php';

use App\info;

$ob = new info();
$data = $ob->prepare($_GET)->show();

if (isset($data) && !empty($data)){
//echo "<pre>";
//print_r($data);
?>
<br><br>
<table border="10">
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Gender</th>
    </tr>

    <tr>
        <td><?php echo $data["id"] ?></td>
        <td><?php echo $data["name"] ?></td>
        <td><?php echo $data["gender"] ?></td>
    </tr>
</table>
<br><br>
<button type="submit"><a href="index.php">HOME</a></button>
<?php 
    }else {
        $_SESSION["mes"] = "<h1>Never try like this again..</h1><br>";
        header('location:404.php');
    }
?>
